**Sistema de Alarma Casera Hecho con Arduino**

El presente codigo realiza la funcion de monitorear una habitacion con uno o dos sensores PIR
Este sistema puede ser ampliado usando un Arduino MEGA en caso de necesitar más sensores o botones
logicamente el codigo para manejar los nuevos agregados tiene que ser desarrollado reciclando el codigo
previamente creado.

Como proyecto de feria de ciencias esta genial, para un sistema mas formal es necesario
adquirir uno mas profesional. No me hago responsable si es usado para proteger una casa y este llegase a
fallar o entregase un falso positivo.

*Lista de materiales*

---

## ELECTRONICOS

1. Placa Arduino UNO. x 1
2. Sensores PIR. x 2
3. LEDS. x 6 
4. Alarma de 12V

## COMPONENTES PASIVOS

1. Resistencias por cada LED 330 Ohm x 1/2 W x6
2. Reistencias de 4k7, ó 4.7K ohm x1/2 W. x 3
3. Tranformador 120V ac a 12V ac (en paises donde el voltaje residencial sea mayor usar uno que cumpla con el voltaje secundario.
4. Protoboard

## COMPONENTES MECANICOS
1. Boton pulsador de 2 patas x 3
2. Rele de 5v (comprar uno ya armado) x 2

---

## Procedimientos

siguiendo los comentarios del codigo conectar los componentes con el arduino desconectado de la
computadora respetando las polaridades de los led y los relé conectando los pines de alimentacion
y tierra correctamente los 5v del arduino conectarlo en la protoboard a la linea roja misma que
servira para alimentar a los relé conectando un cable jumper macho macho a modo que el pin del
rele quede energizado, el pin marcado con GND conectarlo a la linea azul de la protoboard y el pin
GND de arduino de las lineas de la protoboard la roja hacia el pin vcc usando un cable jumper
macho hembra, con gnd igual, salvo que este va conectado a la linea azul pronto subire imagenes
de fritzz mostrando el ejemplo de como quedaria asi mismo como los diagramas.