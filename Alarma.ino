#include <Arduino.h>
const int PinReleSpeaker = 13; //PinReleSpeak er es el que controla la alarma sonora
const int PinReleFocos = 10;// controla el rele que gobierna los focos con el relé
const int PinPir01 = 8; // pines de monitoreo con el pir
int BtnRojo = 2;// controla la activacion al entrar a la habitacion pulsandolo para desarmar la alarma
int BtnAzul = 3;// controla la activacion al salir de la habitacion pulsandolo armará la alarma
int BtnAmarillo = 6; // boton externo para apagado por los seguridad
int LedRojo = 4;// indicador luminoso de alarma activa
int LedVerde = 5;// indicador de alarma desarmada
bool Llave1 = false; // enclavamiento del primer while para verificar la presencia de una persona autorizada
bool Llave2 = false; // persona autorizada ingresada se ignora las alertas emitidas por los pir
bool Llave3 = false; // persona autorizada abandona se da un tiempo a esperar su salida
bool Llave4 = false; // monitoriza todo y activa los focos y alarma en caso de intruso
bool Estado1 = false, Estado2 = false, Estado3 = false;// variables booleanas que solo toman un valor cero o uno HIGH o LOW
int auxiliares1, auxiliares2, auxiliares3;
int auxcal = 0;
bool llavesorda = false;
/**
  los enclavamientos pueden ser modificados o eliminados en el proceso de re estructuracion del programa
*/
void setup() {
  pinMode(PinReleSpeaker, OUTPUT);/*ESTE PIN CONTROLARA EL RELE DE LA BOCINA*/
  pinMode(PinReleFocos, OUTPUT);/*ESTE PIN CONTROLARA EL RELE DE LOS FOCOS*/
  pinMode(PinPir01, INPUT);/**CONTROLA EL MONITOREO DEL PRIMER PIR**/
  pinMode(LedRojo, OUTPUT);/**MANEJA EL LED ROJO DE ALARMA ACTIVA**/
  pinMode(LedVerde, OUTPUT);/**MANEJA EL LED VERDE DE ALARMA APAGADA**/
  pinMode(BtnRojo, INPUT);/**BOTON DE ESTADO DE LA ALARMA(ACTIVADOR)**/
  pinMode(BtnAzul, INPUT);/**BOTON DE ESTADO DE LA ALARMA(DESACTIVADOR)**/
  pinMode(BtnAmarillo, INPUT);/**BOTON QUE DESACTIVA LA ALARMA POR LOS SEGURIDAD**/
  Serial.begin(9600);

}
/*|-----------------------------------------------------------------------------------------|
  |Aca estara colocado el codigo por las funciones Unpir y DosPir y cada una 		    |
  |realizara las acciones declaradas en su interior Unpir estara manejando    		    |
  |un unico pir con el objetivo de ser colocado en un unico circuito sencillo    	    |
  |DosPir auxiliares lo sera las mismas funcionalidades que la primer alarma con la novedad |
  |de estar controlando dos sensores PIR por una unica placa con el manejo de    	    |
  |los mismos componentes de cada una.                                           	    |
  |-----------------------------------------------------------------------------------------|*/
void blinkLedRojo(/* arguments */) {
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo,HIGH);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
  delay(100);
  digitalWrite(LedRojo, HIGH);
  delay(100);
  digitalWrite(LedRojo, LOW);
}//fin blink rojo

void blinkLedVerde(/* arguments */) {
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
  delay(200);
  digitalWrite(LedVerde, LOW);
  delay(200);
  digitalWrite(LedVerde, HIGH);
}//fin blink verde

void armado() {
  digitalWrite(LedVerde, HIGH);
  delay(90);
  digitalWrite(LedVerde, LOW);
  Serial.println("Armado");
  digitalWrite(LedRojo, HIGH);
  delay(90);
  digitalWrite(LedRojo, LOW);
}

void liberarAutorizado_y_Armar() {
  Llave1 = false;
  digitalWrite(LedRojo, LOW);
  digitalWrite(LedVerde, LOW);
  auxiliares1 = 0;
  auxiliares2 = 0;
  auxiliares3 = 0;
  llavesorda = false;
  Estado1 = false;
  Estado2 = false;
  auxcal = 0;

  digitalWrite(LedRojo, HIGH);
  delay(90);
  digitalWrite(LedRojo, LOW);
  digitalWrite(LedRojo, HIGH);
  delay(90);
  digitalWrite(LedRojo, LOW);
  digitalWrite(LedRojo, HIGH);
  delay(90);
  digitalWrite(LedRojo, LOW);
  digitalWrite(LedRojo, HIGH);
  delay(90);
  digitalWrite(LedRojo, LOW);
  delay(5000);

}//fin liberar autorizado y armar

void Unpir() {
  digitalWrite(PinReleSpeaker, HIGH);
  digitalWrite(PinReleFocos, HIGH);
  if (auxcal == 1) {

  } else {
    Serial.println("Calibrando PIRS...");
    delay(15000);
    Serial.println("FINALIZADO...");
    auxcal = 1;
  }
  if (auxcal == 1) {
    Serial.println("MODO UNPIR ACTIVO");
    armado();
    auxiliares1 = digitalRead(PinPir01), BIN;
    Serial.println("FIN LECTURA SENSORES");
    armado();
    delay(5000);
    Serial.println("VERIFICAR");
    armado();
    armado();
    armado();
    if (auxiliares1 == 1) {
      Serial.println("MOVIMIENTO DETECTADO!");
      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(150);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);
        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado

      blinkLedRojo();
      delay(500);
      Estado1 = digitalRead(BtnRojo), BIN;
      delay(250);
      Serial.println("PIN LEIDO ");
      if (Estado1 == 0) {
        digitalWrite(LedRojo, HIGH);
        delay(750);
        digitalWrite(LedVerde, HIGH);

        delay(750);
        digitalWrite(LedRojo, LOW);
        delay(1000);
        digitalWrite(LedVerde, LOW);
        Llave1 = true;
        delay(500);
        /*AUTORIZADO*/
        while (Llave1) {
          auxiliares1 = 0;
          auxiliares2 = 0;
          blinkLedVerde();
          Serial.println("AUTORIZADO");
          delay(1000);
          Estado2 = digitalRead(BtnAzul), BIN;
          delay(250);
          Serial.println("AUTORIZADO");
          delay(250);
          Serial.println("Estado2");
          delay(100);
          Serial.println(Estado2);
          if (Estado2 == 0) {
            liberarAutorizado_y_Armar();
          }
        }
        /*FIN AUTORIZADO*/
      }//fin validador de estado
      if (auxiliares1 == 1 || auxiliares2 == 1) {
      delay(5000);
      Serial.println("5 SEGUNDOS DESPUES");
      delay(500);
      Serial.println("MEDIO SEGUNDO DESPUES");
      llavesorda = true;
      while (llavesorda) {
        digitalWrite(PinReleSpeaker, LOW); //invertir por si configuracion de rele,( en caso de no estar invertido algunos rele al enviar una señal de low se activan y viceversa cuando se envia un HIGH)
        digitalWrite(PinReleFocos, LOW); //invertir por si configuracion de rele.
        delay(250);
        auxiliares3 = digitalRead(BtnRojo), BIN; // BtnAzul Y BtnAmarillo apagan alarma sin atencion
        delay(250);
        auxiliares3 = digitalRead(BtnAmarillo), BIN;
        if (auxiliares3 == 0) {
          llavesorda = false;
          digitalWrite(PinReleSpeaker, LOW); // invertir por si configuracion de rele
          digitalWrite(PinReleFocos, LOW); //invertir por si configuracion de rele
          digitalWrite(LedRojo, LOW);
          delay(250);
        }//FIN Tercer boton
      }//FIN ALARMA SIN ATENCION
    }//INTENTO DE QUE NO SUENE AL RE ARMAR LA ALARMA
    }//END IF DE FIN Y VALIDACION DE SENSORES
    Serial.println("FIN VERIFICAR Y VALIDAR");
  } Serial.println("FIN VALIDAR Y CALIBRAR");

}//fin un pir
void loop() {
  Unpir();
}